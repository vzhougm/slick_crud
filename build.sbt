name := "slick_crud"

version := "0.1"

scalaVersion := "2.13.3"

libraryDependencies += "com.novocode" % "junit-interface" % "0.11" % "test"
libraryDependencies += "com.typesafe.slick" % "slick_2.13" % "3.3.2"
libraryDependencies += "com.typesafe.slick" % "slick-codegen_2.13" % "3.3.2"
libraryDependencies += "com.typesafe.slick" % "slick-hikaricp_2.13" % "3.3.2"
libraryDependencies += "mysql" % "mysql-connector-java" % "8.0.21"
libraryDependencies += "com.google.inject" % "guice" % "4.2.3"
libraryDependencies += "org.slf4j" % "slf4j-nop" % "2.0.0-alpha1"
