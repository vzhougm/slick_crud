package dao

import slick.crud.CRUD
import slick.table.Tables._

/**
 * @author Rubin
 * @version v1 2020/8/26 16:44
 */
class UserDAO extends CRUD[TestUser, TestUserRow] {

  override val database = _
  override val table = TestUser
}
