package slick.table

import slick.crud.Model

// AUTO-GENERATED Slick data model
/** Stand-alone Slick data model for immediate use */
object Tables extends {
  val profile = slick.jdbc.MySQLProfile
} with Tables

/** Slick data model trait for extension, choice of backend or usage in the cake pattern. (Make sure to initialize this late.) */
trait Tables {
  val profile: slick.jdbc.JdbcProfile
  import profile.api._
  import slick.model.ForeignKeyAction
  // NOTE: GetResult mappers for plain SQL are only generated for tables where Slick knows how to map the types of all columns.
  import slick.jdbc.{GetResult => GR}

  /** DDL for all tables. Call .create to execute. */
  lazy val schema: profile.SchemaDescription = TestUser.schema
  @deprecated("Use .schema instead of .ddl", "3.0")
  def ddl = schema

  /** Entity class storing rows of table TestUser
   *  @param fdid Database column fdId SqlType(VARCHAR), PrimaryKey, Length(64,true)
   *  @param fdname Database column fdName SqlType(VARCHAR), Length(255,true), Default(None) */
  case class TestUserRow(fdid: String, fdname: Option[String] = None)
  /** GetResult implicit for fetching TestUserRow objects using plain SQL queries */
  implicit def GetResultTestUserRow(implicit e0: GR[String], e1: GR[Option[String]]): GR[TestUserRow] = GR{
    prs => import prs._
    TestUserRow.tupled((<<[String], <<?[String]))
  }
  /** Table description of table test_user. Objects of this class serve as prototypes for rows in queries. */
    class TestUser(_tableTag: Tag) extends profile.api.Table[TestUserRow](_tableTag, Some("test"), "test_user") with Model{
    def * = (fdId, fdName) <> (TestUserRow.tupled, TestUserRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = ((Rep.Some(fdId), fdName)).shaped.<>({r=>import r._; _1.map(_=> TestUserRow.tupled((_1.get, _2)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column fdId SqlType(VARCHAR), PrimaryKey, Length(64,true) */
    override val fdId: Rep[String] = column[String]("fdId", O.PrimaryKey, O.Length(64,varying=true))
    /** Database column fdName SqlType(VARCHAR), Length(255,true), Default(None) */
    val fdName: Rep[Option[String]] = column[Option[String]]("fdName", O.Length(255,varying=true), O.Default(None))
  }
  /** Collection-like TableQuery object for table TestUser */
  lazy val TestUser = new TableQuery(tag => new TestUser(tag))
}
