package slick.crud

import slick.lifted.Rep

/**
 * @author Rubin
 * @version v1 2020/8/26 12:50
 */
trait Model {
  val fdId: Rep[String]
}
