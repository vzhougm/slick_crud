package slick.crud

/**
 * @author Rubin
 * @version v1 2020/8/25 11:20
 */
class Page[T](page_ : Int = 1, limit_ : Int = 10) {
  val page: Int = page_
  val limit: Int = limit_
  var totalCount: Int = _
  var totalPage: Int = _
  var data: Seq[T] = _

  def setTotalPage(): Unit = {
    val count = totalCount / limit_
    if (totalCount % limit_ == 0) totalPage = count else totalPage = count + 1
  }

}
